# Photography is my Passion

I found my happiness with Photography. I feel glad whenever I see my camera captured a smiling lady, or a laughing boy. I travel to photograph the magnificent beauty of nature. I am in love with the sea and the sky and I always want to capture their stunning views. They are always calling me and my camera, point the lens on them and hit the shutter. My life has been filled with great memories because of Photography, and I will continue making more today until my final day here on earth.

Find out more of my passion and interests [here](http://buyretwits.com).